import gulp from 'gulp';
import plugins from 'gulp-load-plugins';
import yargs from 'yargs';
import path from 'path';
import fs from 'fs';
import config from './gulp/config.js';
import paths from './gulp/paths.js';
import $d from './gulp/dcoupled.js';
import { Composer } from './gulp/composer.js';

let $ = plugins();

gulp.task('dcoupled:bundle', ()=>{

    const namespace = $d.string.toVarName( yargs.argv.name );

    const nameId = $d.string.toId( namespace );

    const bundlePath = path.join( (yargs.argv.dir || config.get('bundle-path')), namespace );

    const bundleDir = path.join( paths.base, bundlePath );

    try {

        //fail if directory exists
        fs.lstatSync( bundleDir ).isDirectory()

        console.log('\n\nCannot overwrite bundle at existing path ' + bundleDir + '\n\n');

        return;

    } catch(e) { 
        //directory doesn't exist, continue
    }

    //create from templates, then save

    gulp.src( paths.bundleTpl )
        .pipe($.template({
            namespace : namespace,
            id : nameId
        }))
        .pipe($.rename((path)=>{
            path.basename = path.basename.replace('bundle', namespace);
        }))
        .pipe( 
            gulp.dest( 
                bundleDir
            ) 
        );  

    //save autoload path in composer.json  

    let composer = new Composer('composer.json');

    composer.addPath( namespace, bundlePath );

    composer.save();
});

gulp.task('dcoupled:extension', ()=>{

    const namespace = $d.string.toVarName( yargs.argv.name );

    const nameId = $d.string.toId( namespace );

    const extPath = path.join( (yargs.argv.dir || config.get('extension-path')), namespace );

    const extDir = path.join( paths.base, extPath );

    try {

        //fail if directory exists
        fs.lstatSync( extDir ).isDirectory()

        console.log('\n\nCannot overwrite extension at existing path ' + bundleDir + '\n\n');

        return;

    } catch(e) { 
        //directory doesn't exist, continue
    }

    //create from templates, then save

    gulp.src( paths.extensionTpl )
        .pipe($.template({
            namespace : namespace,
            id : nameId
        }))
        .pipe($.rename((path)=>{
            path.basename = path.basename.replace('exName', namespace);
        }))
        .pipe( 
            gulp.dest( 
                extDir
            ) 
        );  

    //save autoload path in composer.json  

    let composer = new Composer('composer.json');

    composer.addPath( namespace + '\\Extension', extPath );

    composer.save();    
})