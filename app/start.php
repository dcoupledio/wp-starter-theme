<?php

$require = require('require.php');

$require('vendor/autoload');

use Decoupled\Core\Extension\CoreExtension;
use Decoupled\Wordpress\Extension\WordpressExtension;
use Titan\Extension\TitanExtension;

$app = dcoupled::app();

$app->uses(
    new CoreExtension(),
    new WordpressExtension(),
    new TitanExtension()
);

$require('app/bundles');
