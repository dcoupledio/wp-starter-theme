<?php


class Dcoupled_Importer{

	protected $dir;

	protected $ds = DIRECTORY_SEPARATOR;

	public function __construct( $dir )
	{
		$this->dir = $dir;
	}

	public function __invoke( $file )
	{
		return $this->import( $file );
	}

	public function import( $file )
	{
		$ds = $this->ds;

		$dir = $this->dir;

		$path = $dir.$ds.ltrim( $file, $ds ).'.php';

		$require = $this;

		return require( $path );
	}
}

return new Dcoupled_Importer( dirname(dirname(__FILE__)) );