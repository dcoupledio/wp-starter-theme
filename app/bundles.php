<?php

$app = dcoupled::app();

$app->when( '$bundle.register' )->uses( function( $bundleCollection ){

	$bundleCollection->add(
		new App\AppBundle(),
        new Decoupled\Post\PostBundle(),
        new Decoupled\Page\PageBundle(),
        new Decoupled\PluginOverride\PluginOverrideBundle()
	);
});
