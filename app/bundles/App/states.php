<?php return function( Decoupled\Core\State\StateRouter $stateRouter ){

    $stateRouter('app.default')
        ->when('$app')
        ->uses(function( $option ){

            echo $option('example', 'example');
        });

    $stateRouter('app.404')
        ->when('error404')
        ->uses(function($out, $scope){

            $scope['message'] = 'Well... This is Awkward';

            return $out('@app/404.html.twig', $scope);
        });
};