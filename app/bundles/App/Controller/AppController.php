<?php namespace App\Controller;

class AppController{

    public function index( $scope, $out )
    {
        $scope['bundle'] = 'app';

        return $out('@app/index.html.twig', $scope);
    }
}