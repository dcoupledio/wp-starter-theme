<?php namespace Titan\Extension;

use Decoupled\Core\Application\ApplicationExtension;

class TitanExtension extends ApplicationExtension{

    public function getName()
    {
        return 'titan.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$titan'] = function(){

            return \TitanFramework::getInstance( '$dcoupled' );
        };

        $app['$option'] = function( $app ){

            return function( $optionName, $defaultValue ) use($app){

                return $app['$titan']->getOption( $optionName ) ?: $defaultValue;
            };
        };
    }
}