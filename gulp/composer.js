import fs from 'fs';
import childProcess from 'child_process';

export class Composer{

    constructor(filePath)
    {
        this.data = JSON.parse(
            fs.readFileSync(filePath)
        );

        this.path = filePath;

        if( !this.data.autoload ) this.data.autoload = {};
    }

    addPath( namespace, path )
    {
        if( !this.data.autoload['psr-4'] ) 
        {
            this.data.autoload['psr-4'] = {};
        }

        this.data.autoload['psr-4'][ namespace + '\\' ] = path;

        return this;
    }

    save()
    {
        fs.writeFileSync( 
            this.path,  
            JSON.stringify( this.data, null, 4 )
        );

        childProcess.execSync( 'composer dump-autoload' );
    }
}