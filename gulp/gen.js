import fs from 'fs';

class Gen{

    constructor( namespace, id, savePath, overwrite )
    {
        this.namespace = namespace;

        this.id = id;

        this.savePath = savePath;

        this.overwrite = !!overwrite;
    }
}