import fs from 'fs';
import paths from './paths.js';
import path from 'path';

let file = fs.readFileSync( path.join( paths.base, 'dcoupled.json' ) );

let options = JSON.parse( file );

class Config{

    constructor( options )
    {
        this.options = options;
    }

    get( optionName, defaultValue )
    {
        return this.options.defaults[optionName] ? this.options.defaults[optionName] : defaultValue;
    }
}

export default new Config( options );