

let str = {

    toVarName : ( string )=>{
        string = string.replace('.', '-').replace('_','-');

        string = string.charAt(0).toUpperCase() + string.slice(1);

        return string.replace(/(\-[a-z])/g, ($1)=>{return $1.toUpperCase().replace(['-'],'');});
    },

    toId : ( string ) => {
        return string.replace(/([A-Z])/g, ($1)=>{return "."+$1.toLowerCase();}).slice(1);
    }
}

export default str;