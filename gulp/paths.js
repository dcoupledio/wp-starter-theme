
import path from 'path';

let base = path.join( __dirname, '../' );

export default {
    base : base,
    bundleTpl  : path.join( __dirname, 'generator', 'bundle/**/*.**' ),
    extensionTpl  : path.join( __dirname, 'generator', 'exName/**/*.**' )
};