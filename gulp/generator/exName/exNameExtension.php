<?php namespace <%= namespace %>\Extension;

use Decoupled\Core\Application\ApplicationExtension;

class <%= namespace %>Extension extends ApplicationExtension{

    public function getName()
    {
        return '<%= id %>.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        /**
         
        $app['<%= id %>.service'] = function(){
    
            return new <%= namespace %>Service();
        }

         */
    }
}