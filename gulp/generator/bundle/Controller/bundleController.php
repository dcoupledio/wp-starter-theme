<?php namespace <%= namespace %>\Controller;

class <%= namespace %>Controller{

    public function index( $scope, $out )
    {
        $scope['bundle'] = '<%= id %>';

        return $out('@<%= id %>/index.html.twig', $scope);
    }
}