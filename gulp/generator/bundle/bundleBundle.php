<?php namespace <%= namespace %>;

use Decoupled\Core\Bundle\BundleInterface;

class <%= namespace %>Bundle implements BundleInterface{

    /**
     * @return     string  The id of the bundle.
     */

    public function getName()
    {
        return '<%= id %>';
    }

    /**
     * @return     string  The Bundle Dir
     */

    public function getDir()
    {
        return dirname(__FILE__);
    }

}
