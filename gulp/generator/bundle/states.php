<?php return function( Decoupled\Core\State\StateRouter $stateRouter ){

    $stateRouter('<%= id %>.default')
        ->when('$app')
        ->uses( '<%= namespace %>\\Controller\\<%= namespace %>Controller@index' );
};